#!/usr/bin/env python
"""Pygame Game"""

import sys
import pygame
from pygame.locals import QUIT

settings = {}
with open("./settings") as settingsfile:
    for line in settingsfile.readlines():
        line = line.replace("\n", "").split("=")
        settings[line[0]] = line[1]

colors = {}
with open("./colors") as settingsfile:
    for line in settingsfile.readlines():
        line = line.replace("\n", "").split("=")
        colors[line[0]] = (int(line[1].split(",")[0]),
                           int(line[1].split(",")[1]),
                           int(line[1].split(",")[2]))

pygame.init()
fpsClock = pygame.time.Clock()
screen = pygame.display.set_mode((int(settings["width"]), int(settings["height"])))
oldkeys = {}

while True:
    screen.fill((255, 0, 0))
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        keys = pygame.key.get_pressed()

    oldkeys = keys

    pygame.display.flip()
    fpsClock.tick(int(settings["fps"]))
